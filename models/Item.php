<?php

namespace app\models;

use DateTimeImmutable;
use Yii;

/**
 * This is the model class for table "items".
 *
 * @property int $id
 * @property string $title
 * @property int $price
 * @property string $datetime
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'price', 'datetime'], 'required'],
            [['price'], 'default', 'value' => null],
            [['price'], 'integer', 'min' => 1],
            [['datetime'], 'validateDateTime'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public function validateDateTime()
    {
        if (!$this->hasErrors()) {

            if (!strtotime($this->datetime)) {
                $this->addError('datetime', 'Datetime is invalid');
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'price' => 'Price',
            'datetime' => 'Datetime',
        ];
    }
}
