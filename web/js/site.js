$(
    () => {
        let tbody = $('tbody');
        let addNewItemButton = $('#new-item-add');
        let addNewItemForm = $('#new-item-form');
        let addNewItemModal = $('#exampleModal');

        // clears and fills table of items
        const reloadItems = () => {
            getItems()
                .then(
                    (items) => {
                        tbody.html('');
                        renderItems(items);
                    }
                )
                .catch(
                    (data) => {
                        alert('Error during getting items');
                        console.log(data);
                    }
                );
        };

        // gets items from the server
        const getItems = () => new Promise(
            (resolve, reject) => {

                $.ajax({
                    type: 'GET',
                    url: getItemsUrl,
                    traditional: true,
                    success: (response) => {
                        resolve(response);
                    },
                    error: (response) => {
                        reject(response.responseJSON);
                    },
                });
            }
        );

        // send item to the server
        const addItem = (data) => new Promise(
            (resolve, reject) => {

                $.ajax({
                    type: 'POST',
                    url: addItemUrl,
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    traditional: true,
                    success: (response) => {
                        resolve(response);
                    },
                    error: (response) => {
                        reject(response.responseJSON);
                    },
                });
            }
        );

        // appends item in a table
        const renderItem = (item) => {
            renderItems([item]);
        };

        // appends items in a table
        const renderItems = (items) => {
            for (let item of items) {
                tbody.append(
                    $(
                        `<tr>
                            <th scope="row">${item.id}</th>
                            <td>${item.title}</td>
                            <td>${item.price}</td>
                            <td>${item.datetime}</td>
                        </tr>`
                    )
                );
            }
        };

        // submits new item form
        const submitNewItem = () => {
            let data = addNewItemForm.serializeArray().reduce(
                (obj, item) => {
                    obj[item.name] = item.value;

                    return obj;
                },
                {}
            );

            addNewItemButton.prop('disabled', true);

            addItem(data)
                .then(
                    (item) => {
                        // renderItem(item);
                        reloadItems();
                        addNewItemModal.modal('hide');
                        $(addNewItemForm.find('input')).val('');
                        $(addNewItemForm.find('input')).removeClass('is-invalid');
                    }
                )
                .catch(
                    (errors) => {
                        $(addNewItemForm.find('input')).removeClass('is-invalid');

                        for (let name of Object.keys(errors)) {
                            let descrElementId = $('input[name="' + name + '"]').attr('aria-describedby');
                            let descrErrorContainer = $(`#${descrElementId}.invalid-feedback`).text(errors[name][0]);

                            $('input[name="' + name + '"]').addClass('is-invalid');
                        }
                    }
                )
                .finally(
                    () => {
                        addNewItemButton.prop('disabled', false);
                    }
                );
        };

        reloadItems();

        addNewItemButton.on('click', submitNewItem);
    }
);
