<?php

namespace app\controllers;

use app\models\Item;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays main page.
     *
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {
        return $this->render('index', []);
    }

    /**
     * Gets all items
     */
    public function actionGetItems()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $this->asJson(Item::find()->orderBy(['datetime' => SORT_ASC])->all());
    }

    /**
     * Adds item
     */
    public function actionAddItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $item = new Item();

        if (!$item->load(Yii::$app->request->post(), '') || !$item->save()) {
            Yii::$app->response->statusCode = 400;

            return $this->asJson($item->getErrors());
        }

        $item->refresh();

        return $this->asJson($item);
    }
}
