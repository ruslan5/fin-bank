FROM yiisoftware/yii2-php:7.4-apache

COPY --chown=www-data . /app/

WORKDIR /app

RUN composer install
