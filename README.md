### Install with Docker
    
Start the container

    docker-compose up -d
    
Then go inside the container

    docker exec -it app sh
    
Run migrations inside the container

    php yii migrate
    
You can then access the application through the following URL:

    http://127.0.0.1:8000

**NOTES:** 
- Minimum required Docker engine version `17.04` for development (see [Performance tuning for volume mounts](https://docs.docker.com/docker-for-mac/osxfs-caching/))
- The default configuration uses a host-volume in your home directory `.docker-composer` for composer caches
