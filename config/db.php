<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=' . getenv('MAIN_DB_HOST') . ';port=' . getenv('MAIN_DB_PORT') . ';dbname=' . getenv('MAIN_DB_NAME'),
    'username' => getenv('MAIN_DB_USER'),
    'password' => getenv('MAIN_DB_PASSWORD'),
    'charset' => 'utf8',
];
