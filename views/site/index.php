<?php

/** @var yii\web\View $this */

$this->title = 'Main page';
?>

<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Price, USD</th>
        <th scope="col">Date and time</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="new-item-form">
                    <div class="form-group">
                        <label for="new-item-title">Title</label>
                        <input type="text" name="title" class="form-control" id="new-item-title" aria-describedby="title-feedback">
                        <div id="title-feedback" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="new-item-price">Price</label>
                        <input type="number" name="price" class="form-control" id="new-item-price" aria-describedby="price-feedback">
                        <div id="price-feedback" class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="new-item-datetime">Date and time</label>
                        <input type="datetime-local" name="datetime" class="form-control" pattern="mm-dd-yyyy hh:mm" id="new-item-datetime" aria-describedby="datetime-feedback">
                        <div id="datetime-feedback" class="invalid-feedback"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="new-item-add">Add</button>
            </div>
        </div>
    </div>
</div>

<nav class="navbar fixed-bottom navbar-light bg-trasparent justify-content-center">
    <button class="btn btn-primary rounded-0 px-6 font-weight-bold mb-2 shadow-sm" data-toggle="modal" data-target="#exampleModal">New item</button>
</nav>
